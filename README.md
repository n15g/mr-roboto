# Mr. Roboto

Demo application involving sarcastic robots.

### Prerequisites

* Java 8

### Running

```
./gradlew bootRun
```
or
```
gradle.bat bootRun
```

Follow the on-screen prompts. Interaction is via stdin/out.

## Running the tests

```
gradle test
```

## Authors

* **Nicholas Armstrong** - https://www.linkedin.com/in/narmstrongchibisoft/
