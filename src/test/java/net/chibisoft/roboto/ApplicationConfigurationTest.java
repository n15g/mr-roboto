package net.chibisoft.roboto;

import org.junit.*;
import org.junit.runner.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.junit4.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RobotConfiguration.class)
public class ApplicationConfigurationTest {

    @Test
    public void contextLoads() {
    }

}
