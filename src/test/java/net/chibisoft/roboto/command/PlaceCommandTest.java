package net.chibisoft.roboto.command;

import net.chibisoft.roboto.brain.spacial.*;
import org.assertj.core.api.*;
import org.junit.*;

public class PlaceCommandTest {
    private SoftAssertions softly = new SoftAssertions();

    @Test
    public void testInstruction1() {
        PlaceCommand placeCommand = new PlaceCommand("PLACE 2,3,NORTH");

        softly.assertThat(placeCommand.getX()).isEqualTo(2);
        softly.assertThat(placeCommand.getY()).isEqualTo(3);
        softly.assertThat(placeCommand.getFacing()).isEqualTo(Direction.NORTH);
        softly.assertAll();
    }

    @Test
    public void testInstruction2() {
        PlaceCommand placeCommand = new PlaceCommand("PLACE 0,4,EAST");

        softly.assertThat(placeCommand.getX()).isEqualTo(0);
        softly.assertThat(placeCommand.getY()).isEqualTo(4);
        softly.assertThat(placeCommand.getFacing()).isEqualTo(Direction.EAST);
        softly.assertAll();
    }

    @Test
    public void testInstructionWithLargeDigits() {
        PlaceCommand placeCommand = new PlaceCommand("PLACE 10,42,SOUTH");

        softly.assertThat(placeCommand.getX()).isEqualTo(10);
        softly.assertThat(placeCommand.getY()).isEqualTo(42);
        softly.assertThat(placeCommand.getFacing()).isEqualTo(Direction.SOUTH);
        softly.assertAll();
    }

    @Test
    public void testInstructionWithSpaces() {
        PlaceCommand placeCommand = new PlaceCommand("PLACE 1 , 4, SOUTH");

        softly.assertThat(placeCommand.getX()).isEqualTo(1);
        softly.assertThat(placeCommand.getY()).isEqualTo(4);
        softly.assertThat(placeCommand.getFacing()).isEqualTo(Direction.SOUTH);
        softly.assertAll();
    }

    @Test(expected = CommandException.class)
    public void testInstructionWithBadDirection() {
        new PlaceCommand("PLACE 1,4,NOWHERE");
    }

    @Test(expected = CommandException.class)
    public void testInstructionWithNoArgs() {
        new PlaceCommand("PLACE");
    }
}