package net.chibisoft.roboto.brain;

import net.chibisoft.roboto.command.*;
import net.chibisoft.roboto.output.*;
import org.junit.*;
import org.junit.runner.*;
import org.mockito.*;
import org.mockito.runners.*;

import static org.assertj.core.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
public class SimpleBrainTest {

    @Mock
    private OutputModule outputModule;

    @InjectMocks
    private Brain brain = new SimpleBrain(outputModule);

    @Test
    public void testMemoryIsAliveOnInstantiation() {
        assertThat(brain.isAlive()).isTrue();
    }

    @Test
    public void testKill() {
        brain.accept(new ShutdownCommand());
        assertThat(brain.isAlive()).isFalse();
    }
}