package net.chibisoft.roboto.brain;

import net.chibisoft.roboto.command.*;
import org.assertj.core.api.*;
import org.junit.*;

public class SimpleCommandParserTest {
    private CommandParser parser = new SimpleCommandParser();

    @Test(expected = CommandException.class)
    public void testUnknownInstruction() {
        parser.apply("BAD COMMAND");
    }

    @Test
    public void testShutdown() {
        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(parser.apply("DOMO ARIGATO MR. ROBOTO")).isInstanceOf(ShutdownCommand.class);
        softly.assertThat(parser.apply("domo arigato mr. roboto")).isInstanceOf(ShutdownCommand.class);
        softly.assertThat(parser.apply("DOMO ARIGATO MR. ROBOTO with extra cheese")).isInstanceOf(ShutdownCommand.class);

        softly.assertAll();
    }

    @Test
    public void testReport() {
        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(parser.apply("REPORT")).isInstanceOf(ReportCommand.class);
        softly.assertThat(parser.apply("report")).isInstanceOf(ReportCommand.class);
        softly.assertThat(parser.apply("reportOrElse")).isInstanceOf(ReportCommand.class);

        softly.assertAll();
    }
}