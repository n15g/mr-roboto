package net.chibisoft.roboto.brain.spacial;

import net.chibisoft.roboto.command.*;
import org.assertj.core.api.*;
import org.junit.*;

import static net.chibisoft.roboto.brain.spacial.Direction.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

public class SpacialCortexTest {
    private SpacialCortex cortex = new SpacialCortex();

    private SoftAssertions softly = new SoftAssertions();

    @Test(expected = CommandException.class)
    public void testPlaceOutOfBoundsXPos() {
        cortex.setX(5);
    }

    @Test(expected = CommandException.class)
    public void testPlaceOutOfBoundsXNeg() {
        cortex.setX(-1);
    }

    @Test(expected = CommandException.class)
    public void testPlaceOutOfBoundsYPos() {
        cortex.setX(5);
    }

    @Test(expected = CommandException.class)
    public void testPlaceOutOfBoundsYNeg() {
        cortex.setY(-1);
    }

    @Test
    public void testTurnLeft() {
        cortex.setFacing(NORTH);
        cortex.turnLeft();
        assertThat(cortex.report().getFacing()).isEqualTo(WEST);
    }

    @Test
    public void testTurnRight() {
        cortex.setFacing(NORTH);
        cortex.turnRight();
        assertThat(cortex.report().getFacing()).isEqualTo(EAST);
    }

    @Test
    public void testMoveNorth() {
        cortex.place(1, 1, NORTH);
        cortex.move();
        softly.assertThat(cortex.report().getX()).isEqualTo(1);
        softly.assertThat(cortex.report().getY()).isEqualTo(2);
        softly.assertThat(cortex.report().getFacing()).isEqualTo(NORTH);
        softly.assertAll();
    }

    @Test
    public void testMoveEast() {
        cortex.place(1, 1, EAST);
        cortex.move();
        softly.assertThat(cortex.report().getX()).isEqualTo(2);
        softly.assertThat(cortex.report().getY()).isEqualTo(1);
        softly.assertThat(cortex.report().getFacing()).isEqualTo(EAST);
        softly.assertAll();
    }

    @Test
    public void testMoveSouth() {
        cortex.place(1, 1, SOUTH);
        cortex.move();
        softly.assertThat(cortex.report().getX()).isEqualTo(1);
        softly.assertThat(cortex.report().getY()).isEqualTo(0);
        softly.assertThat(cortex.report().getFacing()).isEqualTo(SOUTH);
        softly.assertAll();
    }

    @Test
    public void testMoveWest() {
        cortex.place(1, 1, WEST);
        cortex.move();
        softly.assertThat(cortex.report().getX()).isEqualTo(0);
        softly.assertThat(cortex.report().getY()).isEqualTo(1);
        softly.assertThat(cortex.report().getFacing()).isEqualTo(WEST);
        softly.assertAll();
    }

    @Test(expected = CommandException.class)
    public void testBumpNorth() {
        cortex.place(4, 4, NORTH);
        cortex.move();
    }


    @Test(expected = CommandException.class)
    public void testBumpEast() {
        cortex.place(4, 4, EAST);
        cortex.move();
    }

    @Test(expected = CommandException.class)
    public void testBumpSouth() {
        cortex.place(0, 0, SOUTH);
        cortex.move();
    }

    @Test(expected = CommandException.class)
    public void testBumpWest() {
        cortex.place(0, 0, WEST);
        cortex.move();
    }
}