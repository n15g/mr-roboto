package net.chibisoft.roboto.brain.spacial;

import org.assertj.core.api.*;
import org.junit.*;

import static net.chibisoft.roboto.brain.spacial.Direction.*;

public class DirectionTest {
    @Test
    public void testLeftTurn() {
        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(NORTH.left()).isEqualTo(WEST);
        softly.assertThat(WEST.left()).isEqualTo(SOUTH);
        softly.assertThat(SOUTH.left()).isEqualTo(EAST);
        softly.assertThat(EAST.left()).isEqualTo(NORTH);
    }

    @Test
    public void testRightTurn() {
        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(NORTH.right()).isEqualTo(EAST);
        softly.assertThat(EAST.right()).isEqualTo(SOUTH);
        softly.assertThat(SOUTH.right()).isEqualTo(WEST);
        softly.assertThat(WEST.left()).isEqualTo(NORTH);
    }

    @Test
    public void testBoundaryRotations() {
        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(NORTH.left()).isEqualTo(WEST);
        softly.assertThat(WEST.right()).isEqualTo(NORTH);

        softly.assertAll();
    }

    @Test
    public void testSomeMultipleTurns() {
        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(NORTH.left().left()).isEqualTo(SOUTH);
        softly.assertThat(NORTH.right().right()).isEqualTo(SOUTH);

        softly.assertThat(NORTH.left().right()).isEqualTo(NORTH);
        softly.assertThat(NORTH.right().right().left()).isEqualTo(EAST);

        softly.assertThat(SOUTH.right().right()).isEqualTo(NORTH);

        softly.assertThat(WEST.right().right()).isEqualTo(EAST);
        softly.assertThat(WEST.left()).isEqualTo(SOUTH);

        softly.assertAll();
    }

    @Test
    public void testSpinLikeCrazy() {
        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(NORTH.left().left().left().left().left().left().left().left().left().left().left()).isEqualTo(EAST);
        softly.assertThat(NORTH.right().right().right().right().right().right().right().right().right()).isEqualTo(EAST);

        softly.assertAll();
    }
}