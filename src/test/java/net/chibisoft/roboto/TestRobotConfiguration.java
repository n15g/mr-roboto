package net.chibisoft.roboto;

import net.chibisoft.roboto.brain.*;
import net.chibisoft.roboto.command.*;
import net.chibisoft.roboto.output.*;
import org.springframework.context.annotation.*;

/**
 * Robot behavioural configuration.
 */
@Configuration
public class TestRobotConfiguration {

    @Bean
    @Scope("prototype")
    public Robot testRobot() {
        OutputModule outputModule = payload -> {
            //No-op
        };
        Brain brain = new SimpleBrain(outputModule);
        CommandParser parser = new SimpleCommandParser();


        return new Robot(
                outputModule,
                brain,
                parser
        );
    }
}
