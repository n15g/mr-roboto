package net.chibisoft.roboto;

import net.chibisoft.roboto.brain.spacial.*;
import org.assertj.core.api.*;
import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.junit4.*;

import static net.chibisoft.roboto.brain.spacial.Direction.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestRobotConfiguration.class)
public class IntegrationTest6 {

    @Autowired
    private Robot robot;

    @Test
    public void test() {
        robot.command("PLACE 0,0,NORTH");
        robot.command("MOVE");
        robot.command("RIGHT");
        robot.command("MOVE");
        robot.command("LEFT");
        robot.command("MOVE");
        robot.command("RIGHT");
        robot.command("MOVE");
        robot.command("LEFT");
        robot.command("MOVE");
        robot.command("RIGHT");
        robot.command("MOVE");
        robot.command("LEFT");
        robot.command("MOVE");
        robot.command("RIGHT");
        robot.command("MOVE");
        robot.command("LEFT");
        robot.command("MOVE");

        PositionalData location = robot.getLocation();

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(location.getX()).isEqualTo(4);
        softly.assertThat(location.getY()).isEqualTo(4);
        softly.assertThat(location.getFacing()).isEqualTo(NORTH);

        softly.assertAll();
    }
}
