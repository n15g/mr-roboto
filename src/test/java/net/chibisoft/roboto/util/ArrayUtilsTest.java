package net.chibisoft.roboto.util;

import org.junit.*;

import java.util.*;
import java.util.stream.*;

import static org.assertj.core.api.Assertions.*;

public class ArrayUtilsTest {

    private static final String[] TEST_ARRAY = {"A", "B", "C"};

    @Test
    public void testGet() {
        Set<String> results = IntStream
                .range(0, 10000)
                .parallel()
                .mapToObj(i -> ArrayUtils.getRandomEntry(TEST_ARRAY))
                .collect(Collectors.toSet());

        //Testing random things sucks. Just hope we get all entries in 10000 tries.
        assertThat(results).containsExactly("A", "B", "C");
    }

}