package net.chibisoft.roboto.brain;

import net.chibisoft.roboto.brain.spacial.*;
import net.chibisoft.roboto.command.*;
import net.chibisoft.roboto.output.*;
import org.springframework.beans.factory.annotation.*;

import static net.chibisoft.roboto.command.TurnCommand.Side.LEFT;

/**
 * Basic {@link Brain} implementation.
 */
public class SimpleBrain implements Brain {
    private boolean alive = true;
    private SpacialCortex spacialCortex = new SpacialCortex();

    private OutputModule output;

    /**
     * Create a new instance.
     *
     * @param output The output module.
     */
    public SimpleBrain(OutputModule output) {
        this.output = output;
    }

    @Override
    public boolean isAlive() {
        return alive;
    }

    @Override
    public boolean isPlaced() {
        return spacialCortex.report().isPlaced();
    }

    @Override
    public void accept(Command command) {
        if (command instanceof ShutdownCommand) {
            shutdown();
        } else if (command instanceof ReportCommand) {
            report();
        } else if (command instanceof PlaceCommand) {
            place((PlaceCommand) command);
        } else if (command instanceof TurnCommand) {
            turn(((TurnCommand) command).getSide());
        } else if (command instanceof MoveCommand) {
            move();
        }
    }

    @Override
    public PositionalData getPositionalData() {
        return spacialCortex.report();
    }

    private void shutdown() {
        output.transmit("SHUTTING DOWN: What *is* life?");
        alive = false;
    }

    private void report() {
        PositionalData data = getPositionalData();
        if (data.isPlaced()) {
            output.transmit(String.format("REPORT: (%d, %d, %s)", data.getX(), data.getY(), data.getFacing()));
        } else {
            output.transmit("IGNORED: Not placed.");
        }
    }

    private void place(PlaceCommand command) {
        if (isPlaced()) {
            output.transmit("IGNORED: Already placed.");
        } else {
            spacialCortex.place(command.getX(), command.getY(), command.getFacing());
            output.transmit("WILCO");
        }
    }

    private void turn(TurnCommand.Side side) {
        if (isPlaced()) {
            if (side == LEFT) {
                spacialCortex.turnLeft();
            } else {
                spacialCortex.turnRight();
            }
            output.transmit("WILCO");
        } else {
            output.transmit("IGNORED: Not placed.");
        }
    }

    private void move() {
        spacialCortex.move();
        output.transmit("WILCO");
    }
}
