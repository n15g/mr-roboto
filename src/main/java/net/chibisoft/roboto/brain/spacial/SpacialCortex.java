package net.chibisoft.roboto.brain.spacial;

import net.chibisoft.roboto.command.*;

import static net.chibisoft.roboto.brain.spacial.Direction.NORTH;

/**
 * Holds the robot's perception of location, direction, etc.
 */
public class SpacialCortex {
    private static final int X_LOWER_BOUND = 0;
    private static final int Y_LOWER_BOUND = 0;
    private static final int X_UPPER_BOUND = 4;
    private static final int Y_UPPER_BOUND = 4;

    private static final String[] BOUNDS_ERROR_MESSAGES = {
            "I don't want to go there, it's scary.",
            "There's nothing there.",
            "<sarcasm>Sure, I'll get right on that...</sarcasm>.",
            "Too many Grues.",
            "How about... No?"
    };

    private boolean placed;
    private Direction facing = NORTH;
    private int x;
    private int y;

    /**
     * Report on the current positional data, as the robot sees it.
     *
     * @return Positional data.
     */
    public PositionalData report() {
        return new PositionalData(placed, facing, x, y);
    }

    /**
     * Place the robot on the table.
     *
     * @param x      X coordinate.
     * @param y      Y coordinate.
     * @param facing Directional facing.
     */
    public void place(int x, int y, Direction facing) {
        setX(x);
        setY(y);
        setFacing(facing);
        placed = true;
    }

    /**
     * Turn the robot to the left.
     */
    public void turnLeft() {
        setFacing(facing.left());
    }

    /**
     * Turn the robot to the right.
     */
    public void turnRight() {
        setFacing(facing.right());
    }

    /**
     * Move the robot one space forward.
     */
    public void move() {
        int newX = x + facing.getXVector();
        int newY = y + facing.getYVector();

        if (newX < X_LOWER_BOUND || newX > X_UPPER_BOUND
                || newY < Y_LOWER_BOUND || newY > Y_UPPER_BOUND) {
            throw new CommandException("IGNORED: ", BOUNDS_ERROR_MESSAGES);
        }

        x = newX;
        y = newY;
    }

    protected void setX(int x) {
        if (x < X_LOWER_BOUND || x > X_UPPER_BOUND) {
            throw new CommandException("IGNORED: ", BOUNDS_ERROR_MESSAGES);
        }
        this.x = x;
    }

    protected void setY(int y) {
        if (y < Y_LOWER_BOUND || y > Y_UPPER_BOUND) {
            throw new CommandException("IGNORED: ", BOUNDS_ERROR_MESSAGES);
        }
        this.y = y;
    }

    protected void setFacing(Direction facing) {
        this.facing = facing;
    }
}
