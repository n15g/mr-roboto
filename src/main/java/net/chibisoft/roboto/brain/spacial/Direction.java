package net.chibisoft.roboto.brain.spacial;

/**
 * Cardinal directions. North, East, South, Western... or something.
 */
public enum Direction {
    NORTH(0, 1),
    EAST(1, 0),
    SOUTH(0, -1),
    WEST(-1, 0);

    private int xVector;
    private int yVector;

    Direction(int xVector, int yVector) {
        this.xVector = xVector;
        this.yVector = yVector;
    }

    public int getXVector() {
        return xVector;
    }

    public int getYVector() {
        return yVector;
    }

    Direction left() {
        if (ordinal() > 0) {
            return values()[ordinal() - 1];
        }
        return WEST;
    }

    Direction right() {
        if (ordinal() < 3) {
            return values()[ordinal() + 1];
        }
        return NORTH;
    }
}
