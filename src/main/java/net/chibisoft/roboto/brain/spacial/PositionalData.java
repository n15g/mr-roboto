package net.chibisoft.roboto.brain.spacial;

/**
 * Immutable snapshot of the robot's positional data.
 */
public class PositionalData {
    private boolean placed;
    private Direction facing;
    private int x = 0;
    private int y = 0;

    /**
     * Create a new instance.
     *
     * @param placed Whether the robot has been placed or not.
     * @param facing The direction the robot is facing.
     * @param x      Position on the x axis.
     * @param y      Position on the y axis.
     */
    PositionalData(boolean placed, Direction facing, int x, int y) {
        this.placed = placed;
        this.facing = facing;
        this.x = x;
        this.y = y;
    }

    public boolean isPlaced() {
        return placed;
    }

    public Direction getFacing() {
        return facing;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
