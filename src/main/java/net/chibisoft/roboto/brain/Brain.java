package net.chibisoft.roboto.brain;

import net.chibisoft.roboto.brain.spacial.*;
import net.chibisoft.roboto.command.*;

/**
 * A place where a robot keeps it's memory; Spacial awareness data, whether it's alive, etc.
 */
public interface Brain {
    /**
     * @return Is this robot alive? Objectively, we don't support robotic existentialism.
     */
    boolean isAlive();

    /**
     * @return True if the robot has been placed.
     */
    boolean isPlaced();

    /**
     * Accept a command and action it if possible.
     *
     * @param command The command to action.
     */
    void accept(Command command);

    /**
     * Report on the spacial data of the robot.
     *
     * @return Spacial data report.
     */
    PositionalData getPositionalData();
}
