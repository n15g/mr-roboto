package net.chibisoft.roboto.util;

import org.apache.commons.lang3.*;

import java.util.*;

/**
 * Randomize responses from an array of objects.
 */
public class ArrayUtils {
    private static Random rnd = new Random();

    /**
     * Get a random element from an array.
     *
     * @param array The array.
     * @param <T>   Array type.
     * @return Random element.
     */
    public static <T> T getRandomEntry(T[] array) {
        return array[rnd.nextInt(array.length)];
    }
}
