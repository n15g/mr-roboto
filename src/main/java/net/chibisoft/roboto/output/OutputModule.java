package net.chibisoft.roboto.output;

/**
 * Robot output module. Used to communicate with the outside world.
 */
public interface OutputModule {
    /**
     * Transmit the given payload to the outside world.
     *
     * @param payload Transmission payload.
     */
    void transmit(String payload);
}
