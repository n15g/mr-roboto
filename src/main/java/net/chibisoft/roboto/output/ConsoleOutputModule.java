package net.chibisoft.roboto.output;

/**
 * STDOUT. Can't get more standard than that.
 */
public class ConsoleOutputModule implements OutputModule {
    @Override
    public void transmit(String payload) {
        System.out.println(payload);
    }
}
