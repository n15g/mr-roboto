package net.chibisoft.roboto;

import net.chibisoft.roboto.brain.*;
import net.chibisoft.roboto.command.*;
import net.chibisoft.roboto.output.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;

import javax.annotation.*;
import java.util.*;

/**
 * Robot application main class.
 */
@SpringBootApplication
public class Application {

    @Autowired
    private OutputModule outputModule;

    @Autowired
    private Brain brain;

    @Autowired
    private CommandParser commandParser;

    private Scanner scanner = new Scanner(System.in, "UTF-8");

    /**
     * Robot go!
     *
     * @param args ignored
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    /**
     * Main input loop, takes input and hands it off to the command processor.
     */
    @PostConstruct
    public void inputLoop() {
        Robot robot = new Robot(outputModule, brain, commandParser);

        outputModule.transmit("WELCOME:\n" +
                "\n" +
                "COMMANDS:\n" +
                "* PLACE X,Y,{NORTH,EAST,SOUTH,WEST}\n" +
                "* LEFT: Turn left\n" +
                "* RIGHT: Turn right\n" +
                "* MOVE: Move forward\n" +
                "* DOMO ARIGATO MR. ROBOTO: Quit\n");

        while (robot.isAlive()) {
            robot.command(scanner.nextLine());
        }
    }
}
