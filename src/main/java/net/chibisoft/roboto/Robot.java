package net.chibisoft.roboto;

import net.chibisoft.roboto.brain.*;
import net.chibisoft.roboto.brain.spacial.*;
import net.chibisoft.roboto.command.*;
import net.chibisoft.roboto.output.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import javax.annotation.*;

/**
 * The robot.
 */
public class Robot {
    private OutputModule output;

    private Brain brain;

    private CommandParser commandParser;

    /**
     * Create a new Robot.
     *
     * @param output        Output module to use for robot output.
     * @param brain         Robot brain.
     * @param commandParser Command parser to use.
     */
    public Robot(OutputModule output, Brain brain, CommandParser commandParser) {
        this.output = output;
        this.brain = brain;
        this.commandParser = commandParser;
    }

    /**
     * Send a command to the robot;
     *
     * @param instruction The command instruction.
     */
    public void command(String instruction) {
        try {
            Command command = commandParser.apply(instruction);
            brain.accept(command);
        } catch (CommandException ex) {
            output.transmit(ex.getMessage());
        }
    }

    /**
     * @return Whether the robot is alive.
     */
    public boolean isAlive() {
        return brain.isAlive();
    }

    /**
     * @return Robot positional data.
     */
    public PositionalData getLocation() {
        return brain.getPositionalData();
    }
}
