package net.chibisoft.roboto;

import net.chibisoft.roboto.brain.*;
import net.chibisoft.roboto.command.*;
import net.chibisoft.roboto.output.*;
import org.springframework.context.annotation.*;

/**
 * Robot behavioural configuration.
 */
@Configuration
public class RobotConfiguration {

    /**
     * @return The {@link OutputModule} this robot is using.
     */
    @Bean
    public OutputModule outputModule() {
        return new ConsoleOutputModule();
    }

    /**
     * @param outputModule Output module.
     * @return The {@link Brain} this robot is using.
     */
    @Bean
    public Brain brain(OutputModule outputModule) {
        return new SimpleBrain(outputModule);
    }

    /**
     * @return The {@link CommandParser} this robot is using.
     */
    @Bean
    public CommandParser commandParser() {
        return new SimpleCommandParser();
    }

    /*
    NOTE: Disabled on ethical grounds.
     */
//    /**
//     * @return Self awareness engine.
//     */
//    @Bean
//    public SelfAwarenessEngine selfAwareness(){
//        return new LearningCapableSelfAwarenessEngine();
//    }
}
