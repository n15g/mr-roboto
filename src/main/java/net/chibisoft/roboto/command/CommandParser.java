package net.chibisoft.roboto.command;

import java.util.function.*;

/**
 * Parses an instruction string and returns the appropriate command. Should throw a {@link CommandException} if no command can be understood.
 */
public interface CommandParser extends Function<String, Command> {
}
