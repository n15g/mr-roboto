package net.chibisoft.roboto.command;

import net.chibisoft.roboto.brain.spacial.*;

import java.util.regex.*;

import static java.lang.Integer.valueOf;
import static net.chibisoft.roboto.brain.spacial.Direction.NORTH;

/**
 * Issues a shutdown command to the robot, causing the application to exit.
 */
public class PlaceCommand implements Command {
    private int x = 0;
    private int y = 0;
    private Direction facing = NORTH;

    /**
     * Create a new instance, from the given instruction. The command will attempt to parse the intended
     * coordinates and facing from the instruction line.
     *
     * @param instruction The instruction received.
     */
    public PlaceCommand(String instruction) {
        Pattern pattern = Pattern.compile("(\\d*),(\\d*),(\\w*)");
        Matcher matcher = pattern.matcher(instruction.replaceAll("\\s", ""));
        if (matcher.find()) {
            try {
                x = valueOf(matcher.group(1));
                y = valueOf(matcher.group(2));
                facing = Direction.valueOf(matcher.group(3));
            } catch (Exception ex) {
                throw new CommandException("INVALID: Bad PLACE command. Format: X,Y,{NORTH,EAST,SOUTH,WEST}");
            }
        } else {
            throw new CommandException("INVALID: Bad PLACE command. Format: X,Y,{NORTH,EAST,SOUTH,WEST}");
        }
    }

    /**
     * Create a new instance.
     *
     * @param x      Place at this x coordinate.
     * @param y      Place at this y coordinate.
     * @param facing Place with this facing.
     */
    public PlaceCommand(int x, int y, Direction facing) {
        this.x = x;
        this.y = y;
        this.facing = facing;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Direction getFacing() {
        return facing;
    }
}
