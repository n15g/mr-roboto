package net.chibisoft.roboto.command;

import java.util.*;
import java.util.function.*;

import static net.chibisoft.roboto.command.TurnCommand.Side.LEFT;
import static net.chibisoft.roboto.command.TurnCommand.Side.RIGHT;

/**
 * Basic {@link CommandParser} implementation.
 */
public class SimpleCommandParser implements CommandParser {
    private Map<String, Function<String, Command>> library = new HashMap<>();

    private static String[] FAILURE_MESSAGES = {
            "I'm not Siri, please be more specific.",
            "Bad command or file name.",
            "I don't understand you... It's probably your fault.",
            "Try again, maybe next time it will work."
    };

    /**
     * Create a new instance.
     */
    public SimpleCommandParser() {
        library.put("DOMO ARIGATO MR. ROBOTO", instruction -> new ShutdownCommand());
        library.put("REPORT", instruction -> new ReportCommand());
        library.put("PLACE", PlaceCommand::new);
        library.put("LEFT", instruction -> new TurnCommand(LEFT));
        library.put("RIGHT", instruction -> new TurnCommand(RIGHT));
        library.put("MOVE", instruction -> new MoveCommand());
    }

    @Override
    public Command apply(String s) {
        String instruction = s.trim().toUpperCase();

        Optional<Function<String, Command>> result = library.entrySet()
                .stream()
                .filter(entry -> instruction.startsWith(entry.getKey()))
                .map(Map.Entry::getValue)
                .findFirst();

        if (result.isPresent()) {
            return result.get().apply(instruction);
        }

        throw new CommandException("UNKNOWN: ", FAILURE_MESSAGES);
    }
}
