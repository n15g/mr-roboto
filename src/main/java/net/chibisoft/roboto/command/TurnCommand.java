package net.chibisoft.roboto.command;

/**
 * Turn around!
 */
public class TurnCommand implements Command {
    private Side side;

    /**
     * Create a new command.
     *
     * @param side Turn to this side.
     */
    public TurnCommand(Side side) {
        this.side = side;
    }

    public Side getSide() {
        return side;
    }

    /**
     * The side to turn too.
     */
    public enum Side {
        LEFT,
        RIGHT
    }
}
