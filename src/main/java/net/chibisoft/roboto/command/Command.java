package net.chibisoft.roboto.command;

/**
 * Interface for formal commands that can be issued to the robot.
 */
public interface Command {
}
