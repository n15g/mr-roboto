package net.chibisoft.roboto.command;

/**
 * Issues a shutdown command to the robot, causing the application to exit.
 */
public class ShutdownCommand implements Command {

}
