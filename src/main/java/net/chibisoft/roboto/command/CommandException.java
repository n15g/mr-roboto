package net.chibisoft.roboto.command;

import static net.chibisoft.roboto.util.ArrayUtils.*;

/**
 * Thrown if there is an issue parsing a command.
 */
public class CommandException extends RuntimeException {
    /**
     * @param message Exception message.
     */
    public CommandException(String message) {
        super(message);
    }

    /**
     * @param prefix          Prefix to apply to the message.
     * @param failureMessages List of possible messages, one will be picked at random.
     */
    public CommandException(String prefix, String[] failureMessages) {
        this(prefix + getRandomEntry(failureMessages));
    }
}
